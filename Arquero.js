import Guerrero from "./Guerrero";

class Arquero extends Guerrero{
    constructor(nombre,decoradorArma) {
        super(nombre);
        this.decoradorArma=decoradorArma;
        this.dano= 5;
    }
    obtenerDano(){
        if(this.decoradorArma==null)
            return this.dano;
        else
            return this.decoradorArma.obtenerDano();
    }

}
export default Arquero;