import DecoradorArma from "./DecoradorArma";

class Hierro extends DecoradorArma{
    constructor(guerrero){
        super(guerrero);
        this.guerrero=guerrero;
    }
    obtenerDano() {
        this.guerrero.dano= super.obtenerDano() + 10;
        return this.guerrero.dano;
    }
}
export default Hierro;