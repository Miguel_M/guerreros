class Guerrero {
    constructor(nombre){
        this.nombre=nombre;
        this.dano=0;
    }

    nombrarGuerrero(nombre){
        this.nombre=nombre;
    }
    obtenerDano(){
        return this.dano;
    }
}
export default Guerrero;