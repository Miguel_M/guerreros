import Fuego from "../Fuego";

let assert = require('assert');
let expect = require('chai').expect;
let should = require('chai').should();
import Infanteria from '../Infanteria';
import Madera from '../Madera';
import Hierro from '../Hierro';
import Arquero from "../Arquero";

describe('Verificar danho de los guerreros de infanteria', function() {
    var guerreroInfanteria;
    var espada;

    it('El danho base de un guerrero de infanteria deberia ser 10', function () {
        guerreroInfanteria = new Infanteria('Bob');
        expect(guerreroInfanteria.obtenerDano()).equal(10);
    });

    it('El danho de una espada de madera deberia ser 15', function () {
        guerreroInfanteria = new Infanteria('Bob');
        espada = new Madera(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        expect(guerreroInfanteria.obtenerDano()).equal(15);
    });
    it('El danho de una espada de hierro deberia ser 20', function () {
        guerreroInfanteria = new Infanteria('Bob');
        espada = new Hierro(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        expect(guerreroInfanteria.obtenerDano()).equal(20);
    });
    it('El danho de una espada de Fuego deberia ser 40', function () {
        guerreroInfanteria = new Infanteria('Bob');
        espada = new Fuego(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        expect(guerreroInfanteria.obtenerDano()).equal(40);
    });
    it('El danho de una espada de Madera y Hierro deberia ser 25', function () {
        guerreroInfanteria = new Infanteria('Bob');
        espada = new Madera(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        espada = new Hierro(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        expect(guerreroInfanteria.obtenerDano()).equal(25);
    });
    it('El danho de una espada de Madera y Fuego deberia ser 45', function () {
        guerreroInfanteria = new Infanteria('Bob');
        espada = new Madera(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        espada = new Fuego(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        expect(guerreroInfanteria.obtenerDano()).equal(45);
    });
    it('El danho de una espada de Madera, Hierro y Fuego deberia ser 55', function () {
        guerreroInfanteria = new Infanteria('Bob');
        espada = new Madera(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        espada = new Hierro(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        espada = new Fuego(guerreroInfanteria);
        guerreroInfanteria = new Infanteria('Bob', espada);
        expect(guerreroInfanteria.obtenerDano()).equal(55);
    });
});
describe('Verificar danho de los guerreros arquero', function() {
    var guerreroArquero;
    var arco;

    it('El danho base de un guerrero arquero deberia ser 5', function () {
        guerreroArquero = new Arquero('Bob');
        expect(guerreroArquero.obtenerDano()).equal(5);
    });

    it('El danho de un arco de madera deberia ser 10', function () {
        guerreroArquero = new Arquero('Bob');
        arco = new Madera(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        expect(guerreroArquero.obtenerDano()).equal(10);
    });
    it('El danho de un arco de Hierro deberia ser 15', function () {
        guerreroArquero = new Arquero('Bob');
        arco = new Hierro(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        expect(guerreroArquero.obtenerDano()).equal(15);
    });
    it('El danho de un arco de Fuego deberia ser 35', function () {
        guerreroArquero = new Arquero('Bob');
        arco = new Fuego(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        expect(guerreroArquero.obtenerDano()).equal(35);
    });
    it('El danho de un arco de Madera y Hierro deberia ser 20', function () {
        guerreroArquero = new Arquero('Bob');
        arco = new Madera(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        arco = new Hierro(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        expect(guerreroArquero.obtenerDano()).equal(20);
    });
    it('El danho de un arco de Madera y Fuego deberia ser 40', function () {
        guerreroArquero = new Arquero('Bob');
        arco = new Madera(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        arco = new Fuego(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        expect(guerreroArquero.obtenerDano()).equal(40);
    });

    it('El danho de un arco de Madera, Hierro y Fuego deberia ser 50', function () {
        guerreroArquero = new Arquero('Bob');
        arco = new Madera(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        arco = new Hierro(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        arco = new Fuego(guerreroArquero);
        guerreroArquero = new Arquero('Bob', arco);
        expect(guerreroArquero.obtenerDano()).equal(50);
    });


});

