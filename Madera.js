import DecoradorArma from "./DecoradorArma";

class Madera extends DecoradorArma{
    constructor(guerrero){
        super(guerrero);
        this.guerrero=guerrero;
    }
    obtenerDano() {
         this.guerrero.dano= super.obtenerDano() + 5;
        return this.guerrero.dano;
    }
}
export default Madera;