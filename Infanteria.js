import Guerrero from "./Guerrero";

class Infanteria extends Guerrero{
    constructor(nombre,decoradorArma) {
        super(nombre);
        this.decoradorArma=decoradorArma;
        this.dano= 10;
    }
    obtenerDano(){
        if(this.decoradorArma==null){
            return this.dano;
        }
        else
        return this.decoradorArma.obtenerDano();
    }
}
export default Infanteria;