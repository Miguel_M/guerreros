import DecoradorArma from "./DecoradorArma";

class Fuego extends DecoradorArma{
    constructor(guerrero){
        super(guerrero);
        this.guerrero=guerrero;
    }
    obtenerDano() {
        this.guerrero.dano= super.obtenerDano() + 30;
        return this.guerrero.dano;
    }
}
export default Fuego;